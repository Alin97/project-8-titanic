#pragma once

#include "Response.h"
#include "Titanic.h"
#include "DataAccess.h"

class AverageDeadPeopleResponse : public Framework::Response, public DataAccess
{
public:
	AverageDeadPeopleResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};