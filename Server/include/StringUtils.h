#pragma once
#include <string>

class StringUtils
{
public:
	int stringToInt(const std::string& string);
	float stringToFloat(const std::string& string);
};
