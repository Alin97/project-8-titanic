#pragma once

#include "Response.h"
#include "Titanic.h"
#include "DataAccess.h"

class NumberOfKidsAliveResponse : public Framework::Response, public DataAccess
{
public:
	NumberOfKidsAliveResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};