#pragma once

#include "Response.h"
#include "Titanic.h"
#include "DataAccess.h"

class SingleSiblingsLeftResponse : public Framework::Response, public DataAccess
{
public:
	SingleSiblingsLeftResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};