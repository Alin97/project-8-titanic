#pragma once

#include "Response.h"
#include "Titanic.h"
#include "DataAccess.h"

class NumberOfPassengersAliveResponse : public Framework::Response, public DataAccess
{
public:
	NumberOfPassengersAliveResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};