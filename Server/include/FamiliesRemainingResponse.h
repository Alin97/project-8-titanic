#pragma once

#include "Response.h"
#include "Titanic.h"
#include "DataAccess.h"

class FamiliesRemainingResponse : public Framework::Response, public DataAccess
{
public:
	FamiliesRemainingResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};