#pragma once

#include <string>

class Titanic
{
public:
	Titanic(int passengerId, int survived, int pClass, std::string name, std::string sex, int age, int sibSp, int parch, std::string ticket, double fare, std::string cabin, std::string embarked);

	int passengerId, survived, pClass, age, sibSp, parch;
	std::string name, sex, ticket, cabin, embarked;
	double fare;
};
