#pragma once

#include <vector>
#include <Titanic.h>
#include <iostream>

class DataAccess
{
public:

	std::vector<Titanic> read();
};
