#include "..\include\StringUtils.h"

int StringUtils::stringToInt(const std::string & string)
{
	if (string.size() == 0) return 0;
	return std::stoi(string);
}

float StringUtils::stringToFloat(const std::string & string)
{
	if (string.size() == 0) return 0.0f;
	return std::stof(string);
}
