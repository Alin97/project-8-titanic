#include "ResponsesManager.h"

#include "AverageDeadPeopleResponse.h"
#include "NumberOfPassengersAliveResponse.h"
#include "NumberOfKidsAliveResponse.h"
#include "SingleSiblingsLeftResponse.h"
#include "FamiliesRemainingResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("AverageDeadPeople", std::make_shared<AverageDeadPeopleResponse>());
	this->Responses.emplace("NumberOfPassengersAlive", std::make_shared<NumberOfPassengersAliveResponse>());
	this->Responses.emplace("NumberOfKidsAlive", std::make_shared<NumberOfKidsAliveResponse>());
	this->Responses.emplace("SingleSiblingsLeft", std::make_shared<SingleSiblingsLeftResponse>());
	this->Responses.emplace("FamiliesRemaining", std::make_shared<FamiliesRemainingResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
