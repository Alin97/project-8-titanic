#include "DataAccess.h"
#include "Response.h"
#include "StringUtils.h"

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace boost::filesystem;

std::vector<Titanic> DataAccess::read()
{
	ifstream file("..\\..\\Server\\TitanicData.txt");

	std::string data;
	std::vector<Titanic> titanics;

	StringUtils stringUtil;

	while (getline(file, data))
	{
		std::vector<std::string> tokens;

		boost::split(tokens, data, boost::is_any_of(","));

		Titanic titanic = Titanic(stringUtil.stringToInt(tokens[0]), stringUtil.stringToInt(tokens[1]), stringUtil.stringToInt(tokens[2]), tokens[3],
			tokens[4], stringUtil.stringToInt(tokens[5]), stringUtil.stringToInt(tokens[6]), stringUtil.stringToInt(tokens[7]), tokens[8],
			stringUtil.stringToFloat(tokens[9]), tokens[10], tokens[11]);
		titanics.push_back(titanic);

	}

	file.close();
	return titanics;
}
