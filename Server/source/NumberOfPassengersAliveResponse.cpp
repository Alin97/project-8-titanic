#include "NumberOfPassengersAliveResponse.h"

#include <boost/range/numeric.hpp>
#include <boost/beast/http/read.hpp>

NumberOfPassengersAliveResponse::NumberOfPassengersAliveResponse() : Response("NumberOfPassengersAlive")
{

}

std::string NumberOfPassengersAliveResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<Titanic> titanics = this->read();
	int count = 0;
	for each(auto titanic in titanics)
	{
		if (titanic.survived == 0) //0=survived
			count++;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "numberOfPassengersAlive.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Number of passengers alive", std::to_string(count)));
	this->content.push_back(boost::property_tree::ptree::value_type("From a total of", std::to_string(titanics.size())));

	return this->getContentAsString();
}
