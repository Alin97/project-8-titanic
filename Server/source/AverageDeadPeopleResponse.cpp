#include "AverageDeadPeopleResponse.h"

#include <boost/range/numeric.hpp>
#include <boost/beast/http/read.hpp>

AverageDeadPeopleResponse::AverageDeadPeopleResponse() : Response("AverageDeadPeople")
{

}

std::string AverageDeadPeopleResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<Titanic> titanics = this->read();
	std::vector<int> vectorDead;
	for each(auto titanic in titanics)
	{
		if(titanic.survived == 1) //1=dead
			vectorDead.push_back(titanic.age);
	}
	float sum = boost::accumulate(vectorDead, 0);
	this->content.push_back(boost::property_tree::ptree::value_type("File", "averageDeadPeople.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Sum", std::to_string(sum)));
	this->content.push_back(boost::property_tree::ptree::value_type("Number of dead people", std::to_string(vectorDead.size())));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(sum / vectorDead.size())));

	return this->getContentAsString();
}
