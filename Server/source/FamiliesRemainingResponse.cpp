#include "FamiliesRemainingResponse.h"

#include <boost/range/numeric.hpp>
#include <boost/beast/http/read.hpp>

FamiliesRemainingResponse::FamiliesRemainingResponse() : Response("FamiliesRemaining")
{

}

std::string FamiliesRemainingResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<Titanic> titanics = this->read();
	std::vector<std::pair<std::string, std::string>> names;

	for each(auto titanic in titanics)
	{
		if (titanic.survived == 0)//0=survived
		{
			titanic.name = titanic.name.substr(1, titanic.name.size() - 2);
			std::string firstName = titanic.name.substr(0, titanic.name.find(' '));
			
			//Capitalize the first letter of the name
			firstName[0] = toupper(firstName[0]);
			titanic.name[0] = toupper(titanic.name[0]);

			names.push_back(std::make_pair(titanic.name, firstName));
		}
	}

	std::sort(names.begin(), names.end());

	std::vector<std::pair<int, std::string>> numberOfMembers;
	int count = 1;
	int max = -1;
	for (int i = 0; i < names.size() - 1; i++)
	{
		count = 1;
		while (names[i].second == names[i + 1].second)
		{
			count++;
			i++;
		}
		numberOfMembers.push_back(std::make_pair(count, names[i].second));

		//
		if (count >= max)
			max = count;
	}

	//number of families
	//this->content.push_back(boost::property_tree::ptree::value_type("", std::to_string(numberOfMembers.size())));

	std::vector<int> frequency(max+1);

	for each (auto member in numberOfMembers)
		frequency.at(member.first)++;


	this->content.push_back(boost::property_tree::ptree::value_type("File", "familiesRemaining.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Nr. membrii: ", "Nr.familii"));
	for (int i = 1; i < frequency.size(); i++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(i), std::to_string(frequency[i])));
	}

	/*this->content.push_back(boost::property_tree::ptree::value_type("File", "familiesRemaining.txt"));

	for (int i = 0; i < frequency.size(); i++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(frequency[i].first), frequency[i].second));
	}*/

	return this->getContentAsString();
}
