#include "NumberOfKidsAliveResponse.h"

#include <boost/range/numeric.hpp>
#include <boost/beast/http/read.hpp>

NumberOfKidsAliveResponse::NumberOfKidsAliveResponse() : Response("NumberOfKidsAlive")
{

}

std::string NumberOfKidsAliveResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<Titanic> titanics = this->read();
	int countAliveKids = 0;
	int countAllKids = 0;
	for each(auto titanic in titanics)
	{
		if (titanic.survived == 0 && titanic.age <= 18) //0=survived
			countAliveKids++;

		if (titanic.age <= 18)
			countAllKids++;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "numberOfKidsAlive.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Number of kids alive", std::to_string(countAliveKids)));
	this->content.push_back(boost::property_tree::ptree::value_type("From a total of", std::to_string(countAllKids)));

	return this->getContentAsString();
}
