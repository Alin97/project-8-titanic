#include "Titanic.h"

Titanic::Titanic(int passengerId, int survived, int pClass, std::string name, std::string sex, int age, int sibSp,
	int parch, std::string ticket, double fare, std::string cabin, std::string embarked)
{

	this->passengerId = passengerId;
	this->survived = survived;
	this->pClass = pClass;
	this->name = name;
	this->sex = sex;
	this->age = age;
	this->sibSp = sibSp;
	this->parch = parch;
	this->ticket = ticket;
	this->fare = fare;
	this->cabin = cabin;
	this->embarked = embarked;

}