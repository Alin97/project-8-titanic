#include "SingleSiblingsLeftResponse.h"

#include <boost/range/numeric.hpp>
#include <boost/beast/http/read.hpp>

SingleSiblingsLeftResponse::SingleSiblingsLeftResponse() : Response("SingleSiblingsLeft")
{

}

std::string SingleSiblingsLeftResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	std::vector<Titanic> titanics = this->read();
	int countSiblings = 0;
	for each(auto titanic in titanics)
	{
		if (titanic.survived == 0 && titanic.sibSp == 1) //0=survived
			countSiblings++;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("File", "singleSiblingsLeft.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Single siblings left", std::to_string(countSiblings)));

	return this->getContentAsString();
}
