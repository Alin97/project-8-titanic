#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	auto averageDeadPeople = requestsManager.getMap().at("AverageDeadPeople")->getContentAsString();
	auto passengersAlive = requestsManager.getMap().at("NumberOfPassengersAlive")->getContentAsString();
	auto numberOfKidsAlive = requestsManager.getMap().at("NumberOfKidsAlive")->getContentAsString();
	auto singleSiblingsLeft = requestsManager.getMap().at("SingleSiblingsLeft")->getContentAsString();
	auto familiesRemaining = requestsManager.getMap().at("FamiliesRemaining")->getContentAsString();

	int option = 1;

	while(option != 0)
	{
		std::cout << "1. Average age of the people who died." << std::endl
			<< "2. Number of the people who survived." << std::endl
			<< "3. Number of the kids who survived." << std::endl
			<< "4. Number of single siblings. " << std::endl
			<< "5. How manny families remained. " << std::endl
			<< "0. EXIT." << std::endl;

		std::cout << std::endl;
		std::cout << "Please insert your option: ";
		std::cin >> option;
		std::cout << std::endl;

		switch(option)
		{
		case 0:
			std::cout << "Exiting..." << std::endl;
			break;

		case 1:
			std::make_shared<Session>(ioContext)->run(host, port, averageDeadPeople);
			ioContext.run();
			ioContext.reset();
			break;

		case 2:
			std::make_shared<Session>(ioContext)->run(host, port, passengersAlive);
			ioContext.run();
			ioContext.reset();
			break;

		case 3:
			std::make_shared<Session>(ioContext)->run(host, port, numberOfKidsAlive);
			ioContext.run();
			ioContext.reset();
			break;

		case 4:
			std::make_shared<Session>(ioContext)->run(host, port, singleSiblingsLeft);
			ioContext.run();
			ioContext.reset();
			break;

		case 5:
			std::make_shared<Session>(ioContext)->run(host, port, familiesRemaining);
			ioContext.run();
			ioContext.reset();
			break;

		default:
			std::cout << "INVALID OPTION !" << std::endl;
			break;
		}
	}

	system("pause");
	return EXIT_SUCCESS;
}