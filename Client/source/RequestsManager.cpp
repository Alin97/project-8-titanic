#include "RequestsManager.h"

#include "AverageDeadPeopleRequest.h"
#include "NumberOfPassengersAliveRequest.h"
#include "NumberOfKidsAliveRequest.h"
#include "SingleSiblingsLeftRequest.h"
#include "FamiliesRemainingRequest.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("AverageDeadPeople", std::make_shared<AverageDeadPeopleRequest>());
	this->requests.emplace("NumberOfPassengersAlive", std::make_shared<NumberOfPassengersAliveRequest>());
	this->requests.emplace("NumberOfKidsAlive", std::make_shared<NumberOfKidsAliveRequest>());
	this->requests.emplace("SingleSiblingsLeft", std::make_shared<SingleSiblingsLeftRequest>());
	this->requests.emplace("FamiliesRemaining", std::make_shared<FamiliesRemainingRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
