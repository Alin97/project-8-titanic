#pragma once

#include "Request.h"

class NumberOfPassengersAliveRequest : public Framework::Request
{
public:
	NumberOfPassengersAliveRequest();
};