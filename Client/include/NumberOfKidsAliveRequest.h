#pragma once

#include "Request.h"

class NumberOfKidsAliveRequest : public Framework::Request
{
public:
	NumberOfKidsAliveRequest();
};